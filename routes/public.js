const express = require("express");
const router = express.Router();
require("dotenv").config();
const {getAll} = require('../db/conexion');

// Función para mostrar solo el primer nombre de los integrantes
function obtenerPrimerNombre(integrantes) {
    return integrantes.map(integrante => {
        const primerNombre = integrante.nombre.split(" ")[0];
        return {...integrante, nombre: primerNombre};
    })
}

router.use((req, res, next) => {
    res.locals.repositorio = process.env.REPOSITORIO;
    res.locals.nombre = process.env.NOMBRE;
    res.locals.materia = process.env.MATERIA;
    res.locals.matricula = process.env.MATRICULA;
    next();
});

// Ruta localhost:{puerto}
router.get("/", async (request, response) => {
    const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
    const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
    const home = await getAll("select * from home");
    response.render("index", {
        home: home[0],
        integrantes: primerNombreIntegrantes,
    });
});

router.get("/word_cloud/", async (request, response) => {
    const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
    const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
    response.render("word_cloud", {
        integrantes: primerNombreIntegrantes
    });
});

router.get("/curso/", async (request, response) => {
    const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
    const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
    response.render("curso", {
        integrantes: primerNombreIntegrantes
    });
});

/*const matriculas = [...new Set(bd.media.map((item) => item.matricula))];

router.get("/:matricula", async (request, response, next) => {
    const matricula = request.params.matricula;
    const integrantes = await getAll("select * from integrantes");
    const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);
    if (matriculas.includes(matricula)) {
        const integranteFilter = bd.integrantes.filter((integrante) => integrante.matricula === matricula);
        const mediaFilter = bd.media.filter((media) => media.matricula === matricula);
        response.render('integrante', {
            integrante: integranteFilter,
            tipoMedia: bd.tipoMedia,
            media: mediaFilter,
            integrantes: primerNombreIntegrantes
        });
    } else {
        next();
    }
});*/

router.get("/:matricula", async (request, response, next) => {
    const matriculas = (await getAll("select matricula from integrantes where activo = 1 order by orden")).map(obj => obj.matricula);
    const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by orden");
    const matricula = request.params.matricula;
    const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
    const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);

    if (matriculas.includes(matricula)) {
        const integranteFilter = await getAll("select * from integrantes where activo = 1 and matricula = ? order by orden", [matricula]);
        const mediaFilter = await getAll("select * from media where activo = 1 and matricula = ? order by orden", [matricula]);
        response.render('integrante', {
            integrante: integranteFilter,
            tipoMedia: tipoMedia,
            media: mediaFilter,
            integrantes: primerNombreIntegrantes
        });
    } else {
        next();
    }
});

module.exports = router;
