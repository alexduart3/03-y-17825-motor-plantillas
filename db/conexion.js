const {query} = require("express");
const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database(
    "./db/integrantes.sqlite",
    sqlite3.OPEN_READWRITE,
    (error) => {
        if (error) {
            console.error('Error al conectar con la base de datos:', error.message);
        } else {
            console.log('Conexión exitosa con la base de datos.');
            db.run("select * from integrantes")
        }
    }
);

async function getAll(query, params) {
    return new Promise((resolve, reject) => {
        db.all(query, params, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

module.exports = {
    db,
    getAll
};
