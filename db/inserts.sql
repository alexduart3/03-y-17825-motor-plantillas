INSERT INTO integrantes (nombre, apellido, matricula, orden, activo) VALUES
('Alexis Adrián', 'Duarte Flecha', 'Y17825', 1, 1),
('Lennys Monserrth', 'Cantero Franco', 'Y26426', 2, 1),
('Gabriel', 'Garcete', 'Y23865', 3, 1),
('Elvio', 'Martinez', 'Y12858', 4, 1),
('Marco', 'Ortigoza Colman', 'UG0507', 5, 1);

INSERT INTO tipoMedia (nombre, orden, activo) VALUES
('Imagen', 1, 1),
('Youtube', 2, 1),
('Dibujo', 3, 1);

INSERT INTO media (tipoMedia, url, src, matricula, titulo, alt, orden, activo) VALUES
(1, NULL, '/assets/images/imagen-personalidad.jpg', 'Y17825', 'Imagen favorita', 'Imagen de un Golden que me representa', 1, 1),
(2, 'https://www.youtube.com/embed/U1ivmi3_IeI?si=9QjM1bJzS3uUpMYk', NULL, 'Y17825', 'Imagen favorita', 'Imagen de un Golden que me representa', 2, 1),
(3, NULL, '/assets/images/imagen-personalidad.jpg', 'Y17825', 'Imagen favorita', 'Imagen de un Golden que me representa', 3, 1),

(1, NULL, '/assets/images/saturno.jpg', 'UG0507', 'Imagen favorita', 'Imagen de un Golden que me representa', 4, 1),
(2, 'https://www.youtube.com/embed/RW75cGvO5xY', NULL, 'UG0507', 'Imagen favorita', 'Imagen de un Golden que me representa', 5, 1),
(3, NULL, '/assets/images/TERERE.png', 'UG0507', 'Imagen favorita', 'Imagen de un Golden que me representa', 6, 1),

(1, NULL, '/assets/images/melissa.jpg', 'Y12858', 'Imagen favorita', 'Imagen de un Golden que me representa', 7, 1),
(2, 'https://www.youtube.com/embed/VhoHnKuf-HI', NULL, 'Y12858', 'Imagen favorita', 'Imagen de un Golden que me representa', 8, 1),
(3, NULL, '/assets/images/dibujo-Elvio.png', 'Y12858', 'Imagen favorita', 'Imagen de un Golden que me representa', 9, 1),

(1, NULL, '/assets/images/messi_pou.jpeg', 'Y23865', 'Imagen favorita', 'Imagen de un Golden que me representa', 10, 1),
(2, 'https://www.youtube.com/embed/B4LvDiIi128?rel=0', NULL, 'Y23865', 'Imagen favorita', 'Imagen de un Golden que me representa', 11, 1),
(3, NULL, '/assets/images/paint_garcete.jpg', 'Y23865', 'Imagen favorita', 'Imagen de un Golden que me representa', 12, 1),

(1, NULL, '/assets/images/imagen-56.jpeg', 'Y26426', 'Imagen favorita', 'Imagen de un Golden que me representa', 13, 1),
(2, 'https://www.youtube.com/embed/vi6-oF3tsPs?si=AdywmEQJk-ewbUQg', NULL, 'Y26426', 'Imagen favorita', 'Imagen de un Golden que me representa', 14, 1),
(3, NULL, '/assets/images/imagen-57.png', 'Y26426', 'Imagen favorita', 'Imagen de un Golden que me representa', 15, 1);

INSERT INTO home (nombre, titulo, src, alt) VALUES
('FSOCIETY', 'Bienvenidos al grupo', '/assets/images/logo.jpeg', 'Grupo FSOCIETY');
